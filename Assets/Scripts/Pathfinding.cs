﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class Pathfinding {
    public class SearchPoint {
        public int x;
        public int y;
        public SearchPoint parent;

        public float f;
        public float g;
        public float h;

        public SearchPoint (int x, int y) {
            this.x = x;
            this.y = y;

            this.parent = null;
            this.f = 0;
            this.g = 0;
            this.h = 0;
        }
    }

    public static List<Point> generatePath (
        Map map,
        Point start,
        Point end
    ) {
        List<SearchPoint> openSet = new List<SearchPoint>();

        byte[,] openArray = new byte[map.width, map.height];
        byte[,] closedArray = new byte[map.width, map.height];

        SearchPoint searchStart = new SearchPoint(start.x, start.y);
        searchStart.g = 0;
        searchStart.h = calculateManhattanDistance(
            new SearchPoint(searchStart.x, searchStart.y),
            new SearchPoint(end.x, end.y)
        );
        searchStart.f = searchStart.g + searchStart.h;

        openArray[searchStart.x, searchStart.y] = 1;

        openSet.Add(searchStart);

        while (openSet.Count > 0) {
            SearchPoint currentPoint = findBestInSet(openSet);

            if (currentPoint.x == end.x && currentPoint.y == end.y) {
                return reconstructPath(currentPoint);
            }

            openArray[currentPoint.x, currentPoint.y] = 0;
            closedArray[currentPoint.x, currentPoint.y] = 1;

            openSet.Remove(currentPoint);

            List<SearchPoint> neighbours = getNeighbours(map, currentPoint);

            for (int i = 0; i < neighbours.Count; i++) {
                SearchPoint neighbour = neighbours[i];

                if (closedArray[neighbour.x, neighbour.y] == 1) {
                    continue;
                }

                float gTentative = currentPoint.g + 1;

                if (openArray[neighbour.x, neighbour.y] != 1) {
                    openSet.Add(neighbour);

                    openArray[neighbour.x, neighbour.y] = 1;
                } else if (gTentative >= neighbour.g) {
                    continue;
                }

                neighbour.parent = currentPoint;
                neighbour.g = gTentative;
                neighbour.h = calculateManhattanDistance(
                    new SearchPoint(neighbour.x, neighbour.y),
                    new SearchPoint(end.x, end.y)
                );
                neighbour.f = neighbour.g + neighbour.h;
            };
        }

        return new List<Point>();
    }

    private static List<Point> reconstructPath (SearchPoint point) {
        List<Point> path = new List<Point>();

        path.Add(new Point(point.x, point.y));

        while (point.parent != null) {
            path.Add(new Point(point.parent.x, point.parent.y));
            point = point.parent;
        }

        path.Reverse();

        return path;
    }

    private static SearchPoint findBestInSet (List<SearchPoint> openSet) {
        SearchPoint best = openSet[0];

        for (int i = 0; i < openSet.Count; i++) {
            SearchPoint point = openSet[i];

            if (point.f < best.f) {
                best = point;
            }
        }

        return best;
    }

    private static List<SearchPoint> getNeighbours (Map map, SearchPoint currentPoint) {
        var neighbours = new List<SearchPoint>();

        var north = new SearchPoint(
            currentPoint.x,
            currentPoint.y + 1
        );

        var east = new SearchPoint(
            currentPoint.x + 1,
            currentPoint.y
        );

        var south = new SearchPoint(
            currentPoint.x,
            currentPoint.y - 1
        );

        var west = new SearchPoint(
            currentPoint.x - 1,
            currentPoint.y
        );

        if (isSearchPointValid(map, north)) {
            neighbours.Add(north);
        }

        if (isSearchPointValid(map, east)) {
            neighbours.Add(east);
        }

        if (isSearchPointValid(map, south)) {
            neighbours.Add(south);
        }

        if (isSearchPointValid(map, west)) {
            neighbours.Add(west);
        }

        return neighbours;
    }

    private static bool isSearchPointValid (
        Map map,
        SearchPoint point
    ) {
        int mapWidth = map.width;
        int mapHeight = map.height;

        bool xOk = (point.x >= 0 && point.x < mapWidth);
        bool yOk = (point.y >= 0 && point.y < mapHeight);

        if (!xOk || !yOk) {
            return false;
        }

        bool blocked = map.tiles[point.x, point.y].blocked;

        if (blocked) {
            return false;
        }

        return true;
    }

    private static float calculateManhattanDistance (
        SearchPoint start,
        SearchPoint end
    ) {
        return Mathf.Abs(start.x - end.x) +
            Mathf.Abs(start.y - end.y);
    }
}
