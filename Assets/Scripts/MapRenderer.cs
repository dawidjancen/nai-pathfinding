﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRenderer : MonoBehaviour {
	private Transform tilemapTransform;
	public Material[] materials;
	public Transform[,] tilemap;
	public Transform tile;

	public void renderMap (Map map) {
		tilemapTransform = gameObject.transform;

		tilemap = new Transform[map.width, map.height];

		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				Transform newTile = Instantiate(
					tile,
					new Vector3(i, 0, j),
					Quaternion.Euler(0, 0, 0)
				);

				int tileType = map.tiles[i, j].type;

				Renderer renderer = newTile.GetComponent<Renderer>();
				renderer.material = materials[tileType];

				newTile.parent = tilemapTransform;

				tilemap[i, j] = newTile;

				map.tiles[i, j].tileTransform = newTile;
			}
		}
	}
}
