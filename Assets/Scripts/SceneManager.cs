﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
	// Tilemap variables
	GameObject tilemapObject;
	MapRenderer mapRenderer;
	Map map;

    private Transform person;
    private Movement personMovement;
    private GameObject target;

	// Person variables
	public Transform personPrefab;

	void Start () {
		// Tilemap setup
		tilemapObject = GameObject.Find("Map");
		mapRenderer = (MapRenderer)tilemapObject.GetComponent(typeof(MapRenderer));

		map = MapGenerator.generateMap(12, 12);

		mapRenderer.renderMap(map);

		// Person setup
		person = Instantiate(
			personPrefab,
			new Vector3(0, 0.5f, 0),
			Quaternion.Euler(0, 0, 0)
		);

        personMovement = (Movement)person.GetComponent(typeof(Movement));

        target = GameObject.Find("Target");

        InvokeRepeating("CheckIfDestinationReached", 1f, 1f);
	}

	// Update is called once per frame
	void CheckIfDestinationReached () {
        if (personMovement.destinationReached) {
            bool targetFound = false;
            Point targetPoint = new Point(0, 0);

            while (!targetFound) {
                int random1 = Random.Range(0, 12);
                int random2 = Random.Range(0, 12);

                if (map.tiles[random1, random2].blocked == false) {
                    targetFound = true;
                    targetPoint = new Point(random1, random2);

                    target.transform.position = new Vector3(random1, 0.15f, random2);
                }
            }

            List<Point> path = Pathfinding.generatePath(
                map,
                new Point((int)person.position.x, (int)person.position.z),
                targetPoint
            );

            personMovement.setPath(path);
        }
	}
}
