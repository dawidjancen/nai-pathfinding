﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	public List<Point> path;
	public bool destinationReached = true;

	public void setPath (List<Point> path) {
		this.path = path;
		this.destinationReached = false;
	}

	void FixedUpdate () {
		if (path != null) {
			if (path.Count > 0) {
				float speed = 0.1f;

				Transform transform = gameObject.transform;

				Vector3 currentPosition = transform.position;
				Vector3 targetPosition = new Vector3(
					path[0].x,
					transform.position.y,
					path[0].y
				);

				transform.position = Vector3.MoveTowards(
					currentPosition,
					targetPosition,
					speed
				);

				if (transform.position == targetPosition) {
					path.RemoveAt(0);
				}
			} else {
				path = null;
				destinationReached = true;
			}
		}
	}
}
