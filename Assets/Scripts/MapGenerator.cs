﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileTypes : int {
	Dirt,
	Grass,
	Water
}

public class Tile {
	public int x;
	public int y;
	public int type;
	public bool blocked;
	public Transform tileTransform;
	public Tile (int x, int y) {
		this.x = x;
		this.y = y;

		this.type = 0;
		this.blocked = false;
	}

	public Tile (int x, int y, int type) {
		this.x = x;
		this.y = y;
		this.type = type;

		this.blocked = false;
	}
}

public class Map {
	public int width;
	public int height;
	public Tile[,] tiles;

	public Map (int width, int height) {
		this.width = width;
		this.height = height;
		this.tiles = new Tile[width, height];
	}

	public void createTile(int x, int y) {
		this.tiles[x, y] = new Tile(x, y, (int)TileTypes.Dirt);
	}

	public Tile getTile(int x, int y) {
		return this.tiles[x, y];
	}
}

static class MapGenerator {
	public static Map generateMap (int width, int height) {
		var map = new Map(width, height);

		map = populateMap(map);
		// map = voronoiDiagramTileTypeGenerator(map);
        map = generateMaze(map);
		map = updateTilePropertyBlocked(map);

		return map;
	}

	private static Map populateMap (Map map) {
		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				map.createTile(i, j);
			}
		}

		return map;
	}

	private static Map randomTileTypeChange (Map map) {
		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				int random = Random.Range(0, 10);

				if (random < 6) {
					map.tiles[i, j].type = (int)TileTypes.Dirt;
				} else if (random < 9) {
					map.tiles[i, j].type = (int)TileTypes.Grass;
				} else {
					map.tiles[i, j].type = (int)TileTypes.Water;
					map.tiles[i, j].blocked = true;
				}
			}
		}

		return map;
	}

    private static Map generateMaze (Map map) {
        for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				map.tiles[i, j].type = (int)TileTypes.Grass;
			}
		}

        for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
                if (j == 1) {
                    if (i < 4) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (i == 5) {
                    if (j < 6) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (j == 1) {
                    if (i < 12 && i > 6) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (i == 10) {
                    if (j > 2) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (i == 7) {
                    if (j > 1 && j < 11) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                map.tiles[9, 3].type = (int)TileTypes.Water;
                map.tiles[8, 5].type = (int)TileTypes.Water;
                map.tiles[9, 7].type = (int)TileTypes.Water;
                map.tiles[8, 9].type = (int)TileTypes.Water;

                if (i == 3) {
                    if (j > 1 && j < 11) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (i == 5) {
                    if (j > 6) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }

                if (i == 1) {
                    if (j > 2) {
                        map.tiles[i, j].type = (int)TileTypes.Water;
                    }
                }
			}
		}

		return map;
    }

	private static Map voronoiDiagramTileTypeGenerator (Map map) {
		int pointsCount = 20;
		int dirtCount = 7;
		int grassCount = 11;
		int waterCount = 2;

		List<Point> points = new List<Point>();
		List<int> tileTypes = new List<int>();

		while (dirtCount > 0) {
			tileTypes.Add((int)TileTypes.Dirt);

			dirtCount--;
		}

		while (grassCount > 0) {
			tileTypes.Add((int)TileTypes.Grass);

			grassCount--;
		}

		while (waterCount > 0) {
			tileTypes.Add((int)TileTypes.Water);

			waterCount--;
		}

		while (pointsCount > 0) {
			var x = Random.Range(0, map.width);
			var y = Random.Range(0, map.height);

			Point point = new Point(x, y);

			if (points.Exists(
				p => p.x == point.x && p.y == point.y
			)) {
				continue;
			}

			points.Add(point);

			pointsCount--;
		}

		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				Point currentPoint = new Point(i, j);

				Point closestPoint = null;
				float closestDistance = 0;
				int closestIndex = 0;

				for (int k = 0; k < points.Count; k++) {
					Point listPoint = points[k];

					float distance = getDistanceBetweenTiles(
						currentPoint,
						listPoint
					);

					float tieBreaker = Random.Range(-0.0001f, 0.0001f);

					if (tieBreaker == 0) {
						tieBreaker = 0.0001f;
					}

					distance = distance + tieBreaker;

					if (closestPoint == null) {
						closestPoint = listPoint;
						closestDistance = distance;

						continue;
					}

					if (distance > closestDistance) {
						continue;
					}

					closestPoint = listPoint;
					closestDistance = distance;
					closestIndex = k;
				}

				map.tiles[i, j].type = tileTypes[closestIndex];
			}
		}

		return map;
	}

	private static Map updateTilePropertyBlocked (Map map) {
		for (int i = 0; i < map.width; i++) {
			for (int j = 0; j < map.height; j++) {
				Tile tile = map.tiles[i, j];

				if (tile.type != (int)TileTypes.Water) {
					continue;
				}

				tile.blocked = true;
			}
		}

		return map;
	}

	private static float getDistanceBetweenTiles (Point start, Point end) {
		float distance = Mathf.Sqrt(
			Mathf.Pow(end.x - start.x, 2) + Mathf.Pow(end.y - start.y, 2)
		);

		return distance;
	}
}
